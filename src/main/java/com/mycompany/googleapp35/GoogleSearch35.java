
package com.mycompany.googleapp35;
import java.io.*;
import java.util.Scanner;
 
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import java.net.URLEncoder;  
 
/**
 * @author slavhare
 * 
 */
 
public class GoogleSearch35 {
    public static final String GOOGLE_SEARCH_URL = "http://www.google.com/search";
	public static void main(String[] args) throws IOException {
 
		try {
			
			// create HTTP Client
			HttpClient httpClient = HttpClientBuilder.create().build();
                        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the search term.");
       
        String search = scanner.nextLine();
        String searchTerm=URLEncoder.encode(search, "UTF-8");
       
        String searchURL = GOOGLE_SEARCH_URL + "?q="+searchTerm+"&num="+3;
 
			// Create new getRequest with above mentioned URL
			HttpGet getRequest = new HttpGet(searchURL);
 
			// Add additional header to getRequest which accepts application/xml data
			getRequest.addHeader("accept", "application/xml");
 
			// Execute your request and catch response
			HttpResponse response = httpClient.execute(getRequest);
                    
			// Check for HTTP response code: 200 = success
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}
 
			// Get-Capture Complete application/xml body response
                       
                      	BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
			String output;
                        System.out.println("============Output:============");
                      
			// Simply iterate through XML response and show on console.
			while ((output= br.readLine()) != null) 
                        {
                            System.out.println(output);
                           
                        }
                         
		} catch (ClientProtocolException e) {
		} catch (IOException e) {
		}
	}
}


